 function Skin(options) {

	this.config = {
		targetElem                :  '.targetElem',
		link                      :  '#themeLink'
	};
	this.cache = {
		defaultList        : ['black','darkblue','darkgrey','green','lightblue','lightgreen','orange','red']
	};

	this.init(options);
 }

 Skin.prototype = {

	constructor: Skin,
	init: function(options) {
		this.config = $.extend(this.config,options || {});
		var self = this,
			_config = self.config;

		$(_config.targetElem).each(function(index,item) {

			$(item).unbind('click');
			$(item).bind('click',function(){
				var attr = $(this).attr('data-value');
				self._doSthing(attr);
			});
		});

		if(window.navigator.userAgent.indexOf("Chrome") !== -1) {
			var tempCookeie = self._loadStorage("skinName"),
				t;
			if(tempCookeie != "null") {
				t = tempCookeie;
			}else {
				t = 'black';
			}
			self._setSkin(t);

		}else {
			var tempCookeie = self._getCookie("skinName");
			self._setSkin(tempCookeie);
		}

	},

	_doSthing: function(attr) {
		var self = this,
			_config = self.config,
			_cache = self.cache;
		if(window.navigator.userAgent.indexOf("Chrome") !== -1) {
			self._doStorage(attr);
			var istrue = localStorage.getItem(attr);
			self._setSkin(attr);
		}else {
			var istrue = self._getCookie(attr);
			if(istrue) {
				for(var i = 0; i < _cache.defaultList.length; i++) {
					if(istrue == _cache.defaultList[i]) {
						self._setSkin(_cache.defaultList[i]);
					}
				}
			}
		}

	},

	_setSkin: function(skinValue){

		var self = this,
			_config = self.config;

		if(skinValue) {
			$(_config.link).attr('href',"/static/Easyui/themes/2019/css/style/"+skinValue+".css");
		}
		if(window.navigator.userAgent.indexOf("Chrome") !== -1) {
			self._saveStorage(skinValue);
		}else {
			self._setCookie("skinName",skinValue,7);
		}

	},

	_doStorage: function(attr) {
		var self = this;
		self._saveStorage(attr);
	},

	_loadStorage: function(attr) {
		var str = localStorage.getItem(attr);
		return str;
	},

	_saveStorage:function(skinValue) {
		var self = this;
		localStorage.setItem("skinName",skinValue);
	},

	_getCookie: function(name) {
		var self = this,
			_config = self.config;
		var arr = document.cookie.split("; ");
		for(var i = 0; i < arr.length; i+=1) {
			var prefix = arr[i].split('=');
			if(prefix[0] == name) {
				return prefix[1];
			}
		}
		return name;
	},

	_setCookie: function(name,value,days) {
		var self = this;

		if (days){
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toGMTString();
		}else {
			var expires = "";
		}
		document.cookie = name+"="+value+expires+"; path=/";
	},

	_removeCookie: function(name) {
		var self = this;

		self._setCookie(name,1,1);
	}
};

$(function(){
	new Skin({});
});
