<?php
use Illuminate\Database\Capsule\Manager as DB;
class Message{

    #获取消息类型及短信模板
    private static function getTemplate($type){
        $template = ['code'=>'', 'title'=>'', 'content'=>''];
        switch ($type){
            case 1:
                #注册
                $template['code']   ='SMS_142355175';
                $template['sms']    =1;
                $template['mail']   =0;
                $template['push']   =0;
                break;
            case 2:
                #手机验证码登陆
                $template['code']   ='SMS_151773866';
                $template['sms']    =1;
                $template['mail']   =0;
                $template['push']   =0;
                break;
            case 3:
                #修改登陆密码
                $template['code']   ='SMS_142355174';
                $template['sms']    =1;
                $template['mail']   =0;
                $template['push']   =0;
                break;
            case 4:
                #修改支付密码
                $template['code']   ='SMS_150735504';
                $template['sms']    =1;
                $template['mail']   =0;
                $template['push']   =0;
                break;
            case 5:
                #邀请注册成功
                $template['code']   ='SMS_135525176';
                $template['title']	='邀请注册成功';
                $template['content']='您的好友已经注册成功，感谢您对平台的支持';
                $template['sms']    =0;
                $template['mail']   =1;
                $template['push']   =1;
                break;
            case 6:
                #客户下单成功
                $template['code']   ='SMS_148866894';
                $template['title']	='新维修单';
                $template['content']='有客户给您发布了新的维修单，请注意查收！';
                $template['sms']    =0;
                $template['mail']   =0;
                $template['push']   =1;
                break;
            case 7:
                #客户申请退款
                $template['code']   ='';
                $template['title']	='申请退款';
                $template['content']='有客户订单申请退款，请注意查收。';
                $template['sms']    =0;
                $template['mail']   =1;
                $template['push']   =1;
                break;
            case 8:
                #订单退款成功
                $template['code']   ='SMS_148866898';
                $template['title']	='退款成功';
                $template['content']='您的订单已经退款成功，感谢您的使用';
                $template['sms']    =1;
                $template['mail']   =1;
                $template['push']   =1;
                break;
            case 9:
                #工程师接单通知
                $template['code']   ='SMS_148861943';
                $template['title']	='工程师已接单';
                $template['content']='您的维修单已被工程师接收，请注意查收';
                $template['sms']    =1;
                $template['mail']   =1;
                $template['push']   =1;
                break;
            case 10:
                #提现申请
                $template['code']   ='';
                $template['title']	='申请提现';
                $template['content']='您的提现申请已经提交，请等待审核';
                $template['sms']    =0;
                $template['mail']   =1;
                $template['push']   =1;
                break;
            case 11:
                #发送完工确认码
                $template['code']   ='SMS_148866907';
                $template['title']	='任务确认码';
                $template['content']='您本次任务的确认码：%s，请注意查收';
                $template['sms']    =1;
                $template['mail']   =1;
                $template['push']   =1;
                break;
            case 12:
                #任务完工，客户消息
                $template['code']   ='';
                $template['title']	='维修任务完工';
                $template['content']='您的维修单已经完工，快来给工程师评价一下吧';
                $template['sms']    =0;
                $template['mail']   =1;
                $template['push']   =1;
                break;
            case 13:
                #任务完工，工程师消息
                $template['code']   ='';
                $template['title']	='维修任务完工';
                $template['content']='您的维修任务已完成，任务赏金已发放，请注意查收';
                $template['sms']    =0;
                $template['mail']   =1;
                $template['push']   =1;
                break;
        }
        return $template;
    }

    #统一消息发送给用户 Message::sendMem($members_id, $type);
    public static function sendMem($members_id, $type, $params=[]){
        $members  = DB::table('members')->find($members_id);
        $template = self::getTemplate($type);
        if(!empty($params)) {
            $template['content'] = sprintf($template['content'], $params['code']);
        }
        if($template['mail']==1) {
            self::mail2members($members_id, $template['title'], $template['content']);
        }
        if($template['push']==1&&!empty($members['cid'])) {
            self::pushMsg($members['cid'], $template['content'], $template['title']);
        }
        if($template['sms']==1&&!Yaf_Application::app()->getConfig()->application->debug) {
            if(!empty($params)) {
                self::sms($members['phone'], $type, $params['code']);
            }else{
                self::sms($members['phone'], $type);
            }
        }
    }
    #统一消息发送给工程师 Message::sendEng($engineer_id, $type, $params=[]);
    public static function sendEng($engineer_id, $type, $params=[]){
        $engineer  = DB::table('engineer')->find($engineer_id);
        $template = self::getTemplate($type);
        if(!empty($params)) {
            $template['content'] = sprintf($template['content'], $params['code']);
        }
        if($template['mail']==1) {
            self::mail2engineer($engineer_id, $template['title'], $template['content']);
        }
        if($template['push']==1&&!empty($engineer['cid'])) {
            self::pushMsg($engineer['cid'], $template['content'], $template['title']);
        }
        if($template['sms']==1&&!Yaf_Application::app()->getConfig()->application->debug) {
            self::sms($engineer['phone'], $type);
        }
    }

    #发短信	Message::sms($phone, $type);
    public static function sms($phone='', $type=0, $code=''){
        $template   =self::getTemplate($type);
        if(in_array($type, [1,2,3,4])){
            $code	= rand(1000, 9999);
        }
        $product= 'SIGN';
        $response = (new Aliyun_Sms)->template($template['code'])->phone($phone)->code($code)->product($product)->send();
        if($response->Code=='OK'&&in_array($type, [1,2,3,4])) {
            Cache::set('sms' . $phone, $code);
        }
    }

    #站内信	Message::mail2members($members_id, '支付成功', '您的订单已支付成功，我们会尽快为您发货，感谢您的使用!');
    public static function Mail2members($members_id, $title, $content){
        $rows	=	array(
            'members_id'    =>  $members_id,
            'title'		    =>	$title,
            'content'       =>  $content,
            'status'		=>	0,
            'created_at'	=>	date('Y-m-d H:i:s'),
        );
        DB::table('members_message')->insert($rows);
    }
    #站内信	Message::mail2engineer($engineer_id, '支付成功', '您的订单已支付成功，我们会尽快为您发货，感谢您的使用!');
    public static function mail2engineer($engineer_id, $title, $content){
        $rows	=	array(
            'engineer_id'   =>  $engineer_id,
            'title'		    =>	$title,
            'content'       =>  $content,
            'status'		=>	0,
            'created_at'	=>	date('Y-m-d H:i:s'),
        );
        DB::table('engineer_message')->insert($rows);
    }

    #推送消息 Message::jPush($members_id, '您的订单已支付成功，我们会尽快为您发货，感谢您的使用!');
    public static function jPush($members_id, $title){
        $cid = DB::table('members')->find($members_id)['cid'];
        if(!empty($cid)){
            self::pushMsg($cid, $title);
        }
    }

    #极光推送消息
    public static function pushMsg($cid, $msg, $title){
        $client = new \JPush\Client('3fb58ee28afe8b9c6c834090', 'b326a60ad9f35aca52f9326e');
        $client->push()
            ->setPlatform('all')
            ->addRegistrationId($cid)
            ->setNotificationAlert($msg)
            ->iosNotification($msg, array(
                'sound' => 'widget://res/002.ogg',
                "badge" => '+1',
            ))
            ->androidNotification($msg, array(
                'title' => $title,
                'extras' => array(
                    'sound' => 'widget://res/002.ogg',
                ),
            ))
            ->send();
    }

}