<?php
class BenchmarkPlugin extends Yaf_Plugin_Abstract {
	public function routerStartup(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {		
		if($request->getMethod() === 'OPTIONS'){exit;}
		header("Access-Control-Allow-Origin:*");
        header("Access-Control-Allow-Methods: PUT,POST,GET,OPTIONS,DELETE");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept, Connection, User-Agent, Cookie");
		header('Access-Control-Max-Age:86400000');
		Yaf_Registry::set('benchmark_start', microtime(true));
	}

	public function routerShutdown(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
	}

	public function dispatchLoopStartup(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {

	}

	public function preDispatch(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {

	}

	public function postDispatch(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {

	}

	public function dispatchLoopShutdown(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
		$start = Yaf_Registry::get('benchmark_start');
		Yaf_Registry::del('benchmark_start');
		$time = round(microtime(true) - (float)$start, 5);
		if ($time > 1)
		{
			Log::out('benchmark', 'I', $request->getRequestUri() . ':' . $time . ':' . (memory_get_usage(true) / 1024) . 'kb');
		}		
	}
}
