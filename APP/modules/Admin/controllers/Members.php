<?php

use Illuminate\Database\Capsule\Manager as DB;

/**
 * @name 会员管理
 */
class MembersController extends ACommonController
{
    protected $table = 'members';
    protected $model = 'membersModel';

    public function getAction($returnData = FALSE)
    {
        $keywords = $this->get('keywords');
        if (!empty($keywords)) {
            array_push($this->search, ['username', 'like', "%{$keywords}%", "or"]);
            array_push($this->search, ['phone', 'like', "%{$keywords}%", "or"]);
        }
        $status = $this->get('status', 0);
        if ($status >= 0) {
            array_push($this->condition, ['status', '=', $status]);
        }
        $start_on = $this->get('start_on');
        if (!empty($start_on)) {
            array_push($this->condition, ['created_at', '>=', $start_on.' 00:00:00']);
        }
        $end_on = $this->get('end_on');
        if (!empty($end_on)) {
            array_push($this->condition, ['created_at', '<=', $end_on.' 23:59:59']);
        }
        json(parent::getAction(true));
    }
}
