<?php

use Illuminate\Database\Capsule\Manager as DB;

/**
 * @name 产品目录
 */
class CatalogController extends ACommonController
{
    public $table = 'catalog';

    /**
     * @name 获取目录
     */
    public function getAction($returnData = false)
    {
        $page     = $this->get('page', 1);
        $limit    = $this->get('rows', 10);
        $offset   = ($page - 1) * $limit;
        $sort     = $this->get('sort', 'sortorder');
        $order    = $this->get('order', 'desc');
        $keywords = trim($this->get('keywords', ''));
        $query    = DB::table('catalog')->where('up', '=', 0);
        if ($keywords !== '') {
            $query = $query->where('name', 'like', "%{$keywords}%");
        }
        $total = $query->count();
        $rows  = $query->orderBy($sort, $order)
            ->offset($offset)
            ->limit($limit)
            ->get();
        //if ($rows->isNotEmpty()) {
        //    foreach ($rows as $index => $row) {
        //        $row->children = DB::table('catalog')->where('up', '=', $row->id)->get();
        //    }
        //}
        json(['total' => $total, 'rows' => $rows]);
    }

    public function getchildrenAction()
    {
        $id = $this->get('id');
        $data = DB::table('catalog')->where('up', $id)->get();
        json($data);
    }

    private function getAllChildren(&$rows, $table, $up = 'up', $alias = 'name', $flagPool=[])
    {
        foreach ($rows as $k => &$v) {
            if (in_array($v->id, $flagPool)) {
                $v->checked = 1;
            } else {
                $v->checked = 0;
            }
            $v->children = DB::table($table)->where($up, $v->id)->select('*', 'name as '.$alias)->orderBy('sortorder', 'DESC')->get();
            $this->getAllChildren($v->children, $table, 'up', $alias, $flagPool);
        }
    }

    private function getAllParent($up, &$called)
    {
        $rows =  DB::table($this->table)->find($up);
        array_push($called, $rows->name);
        if($rows->up>0) {
            $this->getAllParent($rows->up, $called);
        }
    }

    /**
     * @name 添加目录
     */
    public function addAction()
    {
        if(!$this->isPost()) {
            $mainCata = DB::table('catalog')->where('up', '=', 0)->orderBy('sortorder', 'desc')->get();
            foreach($mainCata as $k=>&$v){
                $v->children = DB::table('catalog')->where('up', '=', $v->id)->orderBy('sortorder', 'desc')->get();
            }
            $this->_view->assign('mainCata', $mainCata);
            $this->_view->display('catalog/add.html');
        }else{
            parent::addAction();
        }
    }

    /**
     * @name 编辑目录
     */
    public function editAction()
    {
        if(!$this->isPost()) {
            $id = $this->get('id', NULL);
            if (!$id) return FALSE;
            $dataset       = DB::table('catalog')->find(intval($id));
            $this->_view->assign('dataset', json_encode($dataset));
            $mainCata = DB::table('catalog')->where('up', '=', 0)->orderBy('sortorder', 'desc')->get();
            foreach($mainCata as $k=>&$v){
                $v->children = DB::table('catalog')->where('up', '=', $v->id)->orderBy('sortorder', 'desc')->get();
            }
            $this->_view->assign('mainCata', $mainCata);
            $called = [];
            $this->getAllParent($dataset->up, $called);
            $this->_view->assign('upper', implode(' < ', $called));
            $this->_view->display('catalog/edit.html');
        }else{
            parent::editAction();
        }
    }

    /**
     * @name 删除目录
     */
    public function deleteAction()
    {
        do {
            if ($this->method != 'POST') {
                $result = [
                    'ret' => 1,
                    'msg' => '操作失败',
                ];
                break;
            }
            $id = $this->get('id', '');
            if (empty($id)) {
                $result = [
                    'ret' => 2,
                    'msg' => '参数为空',
                ];
                break;
            }
            if(DB::table('catalog')->where('up', $id)->exists()){
                $result = [
                    'ret' => 3,
                    'msg' => '下级菜单非空',
                ];
                break;
            }
            if (DB::table('catalog')->delete($id)) {
                $result = [
                    'ret' => 0,
                    'msg' => '操作成功',
                ];
            } else {
                $result = [
                    'ret' => 4,
                    'msg' => '删除失败',
                ];
            }
        } while (FALSE);
        json($result);
    }

}
