<?php

use Illuminate\Database\Capsule\Manager as DB;

/**
 * @name 产品管理
 */
class ProductsController extends ACommonController
{

    public function indexAction(): void
    {
        if ($this->isPost()) {
            $page     = $this->getPost('page', 1);
            $limit    = $this->getPost('rows', 10);
            $offset   = ($page - 1) * $limit;
            $sort     = $this->getPost('sort', 'created_at');
            $order    = $this->getPost('order', 'asc');
            $status   = $this->get('status', 0);
            $maps     = [];
            $keywords = $this->getPost('keywords', '');
            if ($keywords !== '') {
                array_push($maps, ['title', 'like', "%{$keywords}%"]);
            }
            $maxDb  = DB::table('company')->max('db');
            $query  = DB::table('products_1')->where($maps)->where('status', $status);
            $cat_id = $this->get('cat_id', 0);
            if ($cat_id > 0) {
                $query = $query->where(function ($query) use ($cat_id) {
                    $query->where('cat_level1', '=', $cat_id)
                        ->orWhere('cat_level2', '=', $cat_id)
                        ->orWhere('cat_level3', '=', $cat_id);
                });
            }
            for ($i = 2; $i <= $maxDb; ++$i) {
                $subQuery = DB::table('products_' . $i)->where($maps)->where('status', $status);
                if ($cat_id > 0) {
                    $subQuery = $subQuery->where(function ($subQuery) use ($cat_id) {
                        $subQuery->where('cat_level1', '=', $cat_id)
                            ->orWhere('cat_level2', '=', $cat_id)
                            ->orWhere('cat_level3', '=', $cat_id);
                    });
                }
                $query = $query->unionAll($subQuery);
            }
            $total = $query->count();
            $rows  = $query->orderBy($sort, $order)
                ->offset($offset)
                ->limit($limit)
                ->get();
            foreach ($rows as $k => $v) {
                $v->cat_level1_name = $v->cat_level1 == 0 ? '' : DB::table('catalog')->where('id', $v->cat_level1)->value('name');
                $v->cat_level2_name = $v->cat_level2 == 0 ? '' : DB::table('catalog')->where('id', $v->cat_level2)->value('name');
                $v->cat_level3_name = $v->cat_level3 == 0 ? '' : DB::table('catalog')->where('id', $v->cat_level3)->value('name');
                $com                = DB::table('company')->find($v->company_id);
                $v->company_name    = $com->name;
                $v->db_with_id      = $com->db . '_' . $v->id;
            }
            json(['total' => $total, 'rows' => $rows]);
        } else {
            $this->_view->display('products/index.html');
        }
    }

    public function newseditAction()
    {
        if ($this->isPost()) {
            $status = $this->get('status', 0);
            $db_with_id = $this->get('db_with_id', 0);
            if($db_with_id){
                foreach ($db_with_id as $v){
                    [$db, $id] = explode('_', $v);
                    DB::table('products_' . $db)->where('id', $id)->update(['status' => $status]);
                }
            }else {
                $db     = $this->get('db', 0);
                $id     = $this->get('id', 0);
                DB::table('products_' . $db)->where('id', $id)->update(['status' => $status]);
            }
            ret(0, '操作成功');
        } else {
            $db_with_id = $this->get('id', 0);
            [$db, $id] = explode('_', $db_with_id);
            $dataset          = DB::table('products_' . $db)->where('id', $id)->first();
            $info             = DB::table('products_info_' . $db)->where('products_id', $id)->first();
            $info->images     = explode(',', $info->images);
            $info->params     = json_decode($info->params);
            $dataset->info    = $info;
            $dataset->company = DB::table('company')->find($dataset->company_id);
            $this->_view->assign('dataset', $dataset);
            $this->_view->assign('db', $db);
            $this->_view->assign('id', $id);
            $this->_view->display('products/newsedit.html');
        }
    }

    public function newsGetAction(): int
    {
        $page     = $this->getPost('page', 1);
        $limit    = $this->getPost('rows', 10);
        $offset   = ($page - 1) * $limit;
        $sort     = $this->getPost('sort', 'sortorder');
        $order    = $this->getPost('order', 'desc');
        $keywords = $this->getPost('keywords', '');
        $cat_id   = $this->getPost('cat_id', 0);

        $query = DB::table('news');
        if ($cat_id > 0) {
            $query = $query->where('news.cat_id', '=', $cat_id);
        }
        if ($keywords !== '') {
            $query = $query->where(function ($query) use ($keywords) {
                $query->where('news.title', 'like', "%{$keywords}%")
                    ->orWhere('news.keywords', 'like', "%{$keywords}%");
            });
        }
        $total = $query->count();
        $rows  = $query->join('news_cat', 'news.cat_id', '=', 'news_cat.id')
            ->orderBy($sort, $order)
            ->offset($offset)
            ->limit($limit)
            ->select('news_cat.title as classname', 'news.*')
            ->get();

        json(['total' => $total, 'rows' => $rows]);
    }

    public function newsDeleteAction()
    {
        do {
            $id = $this->get('id', '');
            if (empty($id)) {
                $result = [
                    'code' => '300',
                    'msg'  => '参数为空',
                ];
                break;
            }
            if (DB::table('news_content')->delete($id) && DB::table('news')->delete($id)) {
                $result = [
                    'code' => '200',
                    'msg'  => '操作成功',
                ];
            } else {
                $result = [
                    'code' => '300',
                    'msg'  => '删除失败',
                ];
            }
        } while (FALSE);
        json($result);
    }

}
