<?php
use Illuminate\Database\Capsule\Manager as DB;

/**
 * @name 文章管理
 */
class NewsController extends ACommonController {

	public function classAction(){}

	public function classGetAction() {
		$page   =	$this->getPost('page', 1);
		$limit  =	$this->getPost('rows', 10);
		$offset	=	($page-1)*$limit;
		$sort	=	$this->getPost('sort', 'sortorder');
		$order	=	$this->getPost('order', 'desc');
		$keywords	= $this->getPost('keywords', '');
		$query		= DB::table('news_cat');
		if($keywords!==''){
			$query	=	$query->where('title','like',"%{$keywords}%");
		}else{
			$query	=	$query->where('up','=','0');
		}
		$total		= $query->count();
		$rows 		= $query->orderBy($sort,$order)
							->get();
		if(!$rows->isEmpty()){
		foreach($rows	as	$k=>&$v){
				$v->recordcount=	DB::table('news')->where('cat_id', $v->id)->count();
				$v->children   =	DB::table('news_cat')->where('up', $v->id)->orderBy($sort,$order)->get();
				if(!$v->children->isEmpty()){
				foreach($v->children	as	$k1=>&$v1){
					$v1->recordcount=	DB::table('news')->where('cat_id', $v1->id)->count();
				}}
		}}
		json(['total'=>$total, 'rows'=>$rows]);
    }
	public function classAddAction(){
		$dataset  	= DB::table('news_cat')->where('up','=',0)->get();
		$this->_view->assign('dataset', $dataset);
    }
	public function classInsertAction(){
		do{
			$title		= $this->getPost('title', '');
			$up			= $this->getPost('up', 	0);
			$sortorder	= $this->getPost('sortorder', 0);
			if( empty($title) ){
				$result	= array(
							'code'	=>	'300',
							'msg'		=>	'菜单名称不能为空',
						);
				break;
			}
			$rows	= array(
								'title'		=>	$title,
								'up'		=>	$up,
								'sortorder'	=>	$sortorder,
								'created_at'=>	date('Y-m-d H:i:s'),
					);
			if( DB::table('news_cat')->insert($rows) ){
				$result	= array(
							'code'	=>	'200',
							'msg'		=>	'操作成功',
						);
			}else{
				$result	= array(
							'code'=>	'300',
							'msg'	=>	'数据插入失败',
						);
			}
		}while(FALSE);

		die(json_encode($result));
    }
	public function classEditAction(){
		$dataset  	= DB::table('news_cat')->where('up',0)->get();
		$id			= intval($this->get('id', NULL));
		if($id==NULL){	return false;	}
     	$mymenu  	= DB::table('news_cat')->find($id);
		$this->_view->assign('dataset', $dataset);
		$this->_view->assign('mymenu',  $mymenu);
    }
    public function classUpdateAction(){
		do{
			$id			= $this->getPost('id', '');
			$title		= $this->getPost('title', '');
			$up			= $this->getPost('up', 	0);
			$sortorder	= $this->getPost('sortorder', 0);
			if( empty($id)||empty($title) ){
				$result	= array(
							'code'		=>	'300',
							'msg'		=>	'菜单id与标题不能为空',
						);
				break;
			}
			if( $id==$up ){
				$result	= array(
							'code'		=>	'300',
							'msg'		=>	'上级菜单循环设置.',
						);
				break;
			}
			$rows	=	array(
							'title'		=>	$title,
							'up'		=>	$up,
							'sortorder'	=>	$sortorder,
							'updated_at'=>	date('Y-m-d H:i:s'),
						);
			if( DB::table('news_cat')->where('id','=',$id)->update($rows)!==FALSE ){
				$result	= array(
							'code'		=>	'200',
							'msg'		=>	'操作成功',
						);
			}else{
				$result	= array(
							'code'		=>	'300',
							'msg'		=>	'更新失败',
						);
			}
		}while(FALSE);

		die(json_encode($result));
    }
    public function classDeleteAction(){
		do{
			$id	= $this->get('id', '');
			if( empty($id) ){
				$result	= array(
							'code'	=>	'300',
							'msg'		=>	'参数为空',
						);
				break;
			}
			if(DB::table('news_cat')->delete($id)){
				$result		= array(
							'code'	=>	'200',
							'msg'		=>	'操作成功',
							'navTabId'		=>	'news_cat',
							);
			}else{
				$result		= array(
							'code'	=>	'300',
							'msg'		=>	'删除失败',
							);
			}
		}while(FALSE);

		die(json_encode($result));
    }

	public function newsAction(): void
    {}

	public function newsGetAction(): int
    {
		$page   =	$this->getPost('page', 1);
		$limit  =	$this->getPost('rows', 10);
		$offset	=	($page-1)*$limit;
		$sort	=	$this->getPost('sort',  'sortorder');
		$order	=	$this->getPost('order', 'desc');
		$keywords		= $this->getPost('keywords', '');
		$cat_id	= $this->getPost('cat_id', 0);

		$query	= DB::table('news');
		if($cat_id>0){
			$query	=	$query->where('news.cat_id','=',$cat_id);
		}
		if($keywords!==''){
			$query	=	$query->where(function ($query) use($keywords) {
										$query->where('news.title','like',"%{$keywords}%")
											  ->orWhere('news.keywords','like',"%{$keywords}%");
									});
		}
		$total		= $query->count();
		$rows 		= $query->join('news_cat','news.cat_id','=','news_cat.id')
							->orderBy($sort,$order)
							->offset($offset)
							->limit($limit)
							->select('news_cat.title as classname','news.*')
							->get();

		json(['total'=>$total, 'rows'=>$rows]);
    }
	public function newsAddAction(){
		$news_cat	= DB::table('news_cat')->where('up','=',0)->orderBy('sortorder','DESC')->get();
		if(!$news_cat->isEmpty()){
		foreach($news_cat as $k=>&$v){
			$v->children	=	DB::table('news_cat')->where('up', $v->id)->orderBy('sortorder','DESC')->get();
		}}
		$this->_view->assign('news_cat', 	$news_cat);
    }
	public function newsInsertAction(){
		do{
			if( $this->method!='POST' ){
				$result	= array(
							'code'	=>	'300',
							'msg'	=>	'操作失败',
						);
				break;
			}
			$title			= $this->getPost('title', '');
			$cat_id	= $this->getPost('cat_id', '');
			$keywords		= $this->getPost('keywords', '');
			$author			= $this->getPost('author', '');
			$copyfrom		= $this->getPost('copyfrom', '');
			$copyfromurl	= $this->getPost('copyfromurl', '');
			$sortorder		= $this->getPost('sortorder', '');
			$status			= $this->getPost('status', 		0);
			$recommend		= $this->getPost('recommend',	0);
			$content		= $this->getPost('content', '');
			if( empty($title) || empty($content) ){
				$result	= array(
							'code'	=>	'300',
							'msg'		=>	'标题和内容不能为空',
						);
				break;
			}
			$rows	=	array(
							'cat_id'	=>	$cat_id,
							'title'			=>	$title,
							'keywords'		=>	$keywords,
							'author'		=>	$author,
							'copyfrom'		=>	$copyfrom,
							'copyfromurl'	=>	$copyfromurl,
							'sortorder'		=>	$sortorder,
							'status'		=>	$status,
							'recommend'		=>	$recommend,
							'created_at'	=>	date('Y-m-d H:i:s'),
						);
			$files	= $this->getFiles('upfile', NULL);
			if( $files!=NULL && $files['size']>0 ){
				if( $image = $this->_uploadPictureToCDN('upfile') ){
					$rows['logo']	=	$image;
				}else{
					$result	= array(
						'code'		=>	'300',
						'msg'		=>	'图片上传失败.',
					);
					break;
				}
			}elseif( preg_match('#<img.*?src\=[\"\']([^\"\']*)[\"\']#is', stripslashes($content), $imagesurl) ){
				$rows['logo']	=	$imagesurl[1];
			}
			if( $id=DB::table('news')->insertGetId($rows) ){
				DB::table('news_content')->insert(['id'=>$id,'content'=>$content]);
				$result	= array(
							'code'		=>	'200',
							'msg'		=>	'操作成功',
						);
			}else{
				$result	= array(
							'code'	=>	'300',
							'msg'	=>	'数据插入失败',
						);
			}
		}while(FALSE);
		json($result);
    }
	public function newsEditAction(){
		$id			= $this->get('id', 0);
		$dataset  	= (new newsModel)->with('content')->find(intval($id));
		$this->_view->assign('dataset', $dataset);

		$news_cat	= DB::table('news_cat')->where('up',0)->orderBy('sortorder','DESC')->get();
		if(!$news_cat->isEmpty()){
		foreach($news_cat as $k=>$v){
			$v->children	=	DB::table('news_cat')->where('up',$v->id)->orderBy('sortorder','DESC')->get();
		}}
		$this->_view->assign('news_cat', 	$news_cat);
    }
    public function newsUpdateAction(){
		do{
			$id				= $this->getPost('id', '');
			$title			= $this->getPost('title', '');
			$cat_id	        = $this->getPost('cat_id', '');
			$keywords		= $this->getPost('keywords', '');
			$author			= $this->getPost('author', '');
			$copyfrom		= $this->getPost('copyfrom', '');
			$copyfromurl	= $this->getPost('copyfromurl', '');
			$sortorder		= $this->getPost('sortorder', '');
			$status			= $this->getPost('status', 		0);
			$recommend		= $this->getPost('recommend',	0);
			$content		= $this->getPost('content', '');
			if( empty($title) || empty($content) ){
				$result	= array(
							'code'	=>	'300',
							'msg'		=>	'标题和内容不能为空',
						);
				break;
			}
			$rows	=	array(
							'cat_id'	=>	$cat_id,
							'title'			=>	$title,
							'keywords'		=>	$keywords,
							'author'		=>	$author,
							'copyfrom'		=>	$copyfrom,
							'copyfromurl'	=>	$copyfromurl,
							'sortorder'		=>	$sortorder,
							'status'		=>	$status,
							'recommend'		=>	$recommend,
							'updated_at'	=>	date('Y-m-d H:i:s'),
						);
			$files	= $this->getFiles('upfile', NULL);
			if( $files!=NULL && $files['size']>0 ){
				if( $image = $this->_uploadPictureToCDN('upfile') ){
					$rows['logo']	=	$image;
				}else{
					$result	= array(
						'code'		=>	'300',
						'msg'		=>	'图片上传失败.',
					);
					break;
				}
			}elseif( empty($rows['logo']) &&  preg_match('#<img.*?src\=[\"\']([^\"\']*)[\"\']#is', stripslashes($content), $imagesurl) ){
				$rows['logo']	=	$imagesurl[1];
			}
			if( DB::table('news')->where('id','=',$id)->update($rows)!==FALSE ){
				DB::table('news_content')->where('id','=',$id)->update(['content'=>$content]);
				$result	= array(
							'code'		=>	'200',
							'msg'		=>	'操作成功',
						);
			}else{
				$result	= array(
							'code'	=>	'300',
							'msg'	=>	'数据插入失败',
						);
			}
		}while(FALSE);
		json($result);
    }
	public function newsRecycleAction(){
		do{
			$id	= $this->get('id', '');
			if( empty($id) ){
				$result	= array(
							'code'	=>	'300',
							'msg'		=>	'参数为空',
						);
				break;
			}

			$status	=	DB::table('news')->where('id', $id)->value('status');
			$rows	=	array(
							'status'	=> ($status>0)?0:1,
			);
			if(DB::table('news')->where('id','=',$id)->update($rows)){
				$result		= array(
							'code'		=>	'200',
							'msg'		=>	'操作成功',
							);
			}else{
				$result		= array(
							'code'	=>	'300',
							'msg'		=>	'删除失败',
							);
			}
		}while(FALSE);
		json($result);
    }
    public function newsDeleteAction(){
		do{
			$id	= $this->get('id', '');
			if( empty($id) ){
				$result	= array(
							'code'		=>	'300',
							'msg'		=>	'参数为空',
						);
				break;
			}
			if(DB::table('news_content')->delete($id) && DB::table('news')->delete($id)){
				$result		= array(
							'code'		=>	'200',
							'msg'		=>	'操作成功',
							);
			}else{
				$result		= array(
							'code'		=>	'300',
							'msg'		=>	'删除失败',
							);
			}
		}while(FALSE);
		json($result);
    }

}
