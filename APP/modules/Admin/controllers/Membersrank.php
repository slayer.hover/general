<?php

use Illuminate\Database\Capsule\Manager as DB;

/**
 * @name 会员等级
 */
class MembersrankController extends ACommonController
{
    protected $table = 'members_rank';
    protected $model = 'membersrankModel';

    public function getAction($flag = FALSE)
    {
        $keywords = $this->get('keywords');
        if (!empty($keywords)) {
            array_push($this->condition, ['name', 'like', "%{$keywords}%"]);
        }
        json(parent::getAction(true));
    }

    public function cellUpdateAction()
    {
        do {
            if ($this->method == 'POST') {
                $rows = $this->get('data');
                if (empty($rows) || empty($rows['id'])) {
                    $result = array(
                        'ret' => 1,
                        'msg' => '请传入数据，并确认id值',
                    );
                    break;
                }
                $id = $rows['id'];
                $info = new membersrankModel;
                $key = $info->primaryKey;
                if ($info->where($key, '=', $id)->update($rows) !== FALSE) {
                    $result = array(
                        'ret' => 0,
                        'msg' => '更新成功',
                    );
                } else {
                    $result = array(
                        'ret' => 2,
                        'msg' => '更新失败',
                    );
                }
            } else {
                $result = array(
                    'ret' => 3,
                    'msg' => '提交方式有误',
                );
            }
        } while (FALSE);
        json($result);
    }
}
