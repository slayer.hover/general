<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;

class ordersModel extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'id';
    protected $appends = ['status_name', 'paid_type_name', 'company', 'customer', 'timeslot', 'addtional'];

    public function getCustomerAttribute()
    {
        return DB::table('customer')->select('id', 'phone', 'username', 'created_at')->find($this->attributes['customer_id']);
    }

    public function getTimeslotAttribute()
    {
        return DB::table('auto_timeslot')->find($this->attributes['timeslot_id']);
    }

    public function getAddtionalAttribute()
    {
        if($this->attributes['addtional_id']>0) {
            return DB::table('auto_addtional')->find($this->attributes['addtional_id']);
        }else{
            return [];
        }
    }

    public function getPaidAtAttribute($value)
    {
        return $value == '0000-00-00 00:00:00' ? '未支付' : $value;
    }

    public function getCompanyAttribute()
    {
        return $this->attributes['company_id'] > 0 ? DB::table('company')->find($this->attributes['company_id']) : '';
    }

    public function getStatusNameAttribute()
    {
        $statusName = '';
        switch ($this->attributes['status']) {
            case '100':
                $statusName = '待付款';
                break;
            case '200':
                $statusName = '已支付';
                break;
            case '300':
                $statusName = '已取消';
                break;
            case '400':
                $statusName = '已完成';
                break;
            case '500':
                $statusName = '申请退款';
                break;
            case '600':
                $statusName = '已退款';
                break;
        }
        return $statusName;
    }

    public function getPaidTypeNameAttribute()
    {
        $paidTypeName = '';
        switch ($this->attributes['paid_type']) {
            case '1':
                $paidTypeName = '支付宝';
                break;
            case '2':
                $paidTypeName = '微信支付';
                break;
            default:
                $paidTypeName = '未定';
        }
        return $paidTypeName;
    }

    public static function notifyProcess($type, $params)
    {
        Log::out($type, 'I', "【支付成功】:\n" . json_encode($params, JSON_UNESCAPED_UNICODE) . "\n");
        if (strtoupper($type) == 'ALI') {
            $paid_type = 1;
            $order_no  = $params['out_trade_no'];    //商户订单号_内部订单号
            $amount    = $params['total_amount'] * 100;
            $trade_no  = $params['trade_no'];
        } else {
            $paid_type = 2;
            $order_no  = substr($params["out_trade_no"], 0, 20);
            $amount    = $params['total_fee'];
            $trade_no  = $params['transaction_id'];
        }
        return self::notifyBody($paid_type, $order_no, $amount, $trade_no, $params["out_trade_no"]);
    }

    private static function notifyBody($paid_type, $order_no, $amount, $trade_no, $out_trade_no)
    {
        $orders = DB::table('auto_orders')->where('order_no', '=', $order_no)->first();
        if (empty($orders)) {
            return FALSE;
        }
        if (bccomp($orders->amount * 100, $amount) !== 0) {
            return FALSE;
        }
        if ($orders->status > 100) {
            return TRUE;
        }
        try {
            DB::beginTransaction();
            /***1.更新order表***/
            $rows = [
                'status'         => 200,
                'paid_type'      => $paid_type,
                'paid_at'        => date('Y-m-d H:i:s'),
                'transaction_no' => $trade_no,
                'out_trade_no'   => $out_trade_no,
            ];
            DB::table('auto_orders')->where('order_no', $order_no)->update($rows);
            DB::commit();
            return TRUE;
        } catch (Exception $e) {
            DB::rollBack();
            return FALSE;
        }
    }


}
