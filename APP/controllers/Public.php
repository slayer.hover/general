<?php

use Illuminate\Database\Capsule\Manager as DB;
use SliderCapcha\SliderCapcha;

class PublicController extends CoreController
{
    public function infoAction()
    {
        $row = DB::table('sys_config')->where('type', 1)->pluck('value', 'key');
        json(['ret' => 0, 'msg' => '系统信息', 'data' => $row]);
    }

    public function makeAction()
    {
        (new Captcha())->make();
    }

    public function loginAction()
    {
        if (!$this->isPost()) {
            $this->_view->assign('title', DB::table('sys_config')->where('key', 'name')->value('value'));
            $this->_view->display('public/login.html');
        } else {
            do {
                if (!SliderCapcha::check()) {
                    $result = [
                        'ret' => 1,
                        'msg' => '滑动验证失败.',
                    ];
                    break;
                }
                $inputs = $this->postData;
                $roles  = [
                    'username' => ['role' => 'required|exists:admin.username', 'msg' => '用户名'],
                    'password' => ['role' => 'required', 'msg' => '密码'],
                ];
                Validate::check($inputs, $roles);
                $sysusers = new adminModel();
                if ($sysusers->checkUsername($inputs['username']) == FALSE) {
                    $result = [
                        'ret' => 3,
                        'msg' => '无此账号.',
                    ];
                    break;
                }
                if ($sysusers->checkStatus($inputs['username']) == FALSE) {
                    $result = [
                        'ret' => 4,
                        'msg' => '账号已锁定.',
                    ];
                    break;
                }
                if ($sysusers->checkPassword($inputs['username'], $inputs['password']) == FALSE) {
                    if (!isset($this->session->try_times)) {
                        $this->session->try_times = 0;
                    }
                    $this->session->try_times++;
                    if ($this->session->try_times > 5) {
                        $result = [
                            'ret' => 4,
                            'msg' => '账号已锁定.',
                        ];
                        $rows   = [
                            'lockuntil' => time() + 60 * 20,
                        ];
                        DB::table('admin')->where('username', '=', $inputs['username'])->update($rows);
                        setcookie('lockFlag', 1, time() + 60 * 20);
                        $this->session->del('try_times');
                    } else {
                        $result = [
                            'ret' => 5,
                            'msg' => '密码有误,剩余' . (5 - $this->session->try_times) . '次',
                        ];
                    }
                    break;
                }
                if ($data = $sysusers->setUserLogin($inputs['username'], $inputs['password'])) {
                    $this->session->del('try_times');
                    $result = [
                        'ret'  => 0,
                        'msg'  => '登陆成功.',
                        'data' => $data
                    ];
                } else {
                    $result = [
                        'ret' => 6,
                        'msg' => '登陆失败.',
                    ];
                }
            } while (FALSE);
            json($result);
        }
    }

    public function adminmakeAction()
    {
        $config['dir'] = APP_PATH . '/public/plugins/slide-capcha/bg/';
        $sc            = new SliderCapcha($config);
        $sc->make();
    }

    public function adminloginAction()
    {
        if (!$this->isPost()) {
            $this->_view->assign('title', DB::table('sys_config')->where('key', 'name')->value('value'));
            $this->_view->display('public/adminlogin.html');
        } else {
            do {
                if (!SliderCapcha::check()) {
                    $result = [
                        'ret' => 1,
                        'msg' => '滑动验证失败.',
                    ];
                    break;
                }
                $inputs = $this->postData;
                $roles  = [
                    'username' => ['role' => 'required|exists:admin.username', 'msg' => '用户名'],
                    'password' => ['role' => 'required', 'msg' => '密码'],
                ];
                Validate::check($inputs, $roles);
                $sysusers = new adminModel();
                if ($sysusers->checkUsername($inputs['username']) == FALSE) {
                    $result = [
                        'ret' => 3,
                        'msg' => '无此账号.',
                    ];
                    break;
                }
                if ($sysusers->checkStatus($inputs['username']) == FALSE) {
                    $result = [
                        'ret' => 4,
                        'msg' => '账号已锁定.',
                    ];
                    break;
                }
                if ($sysusers->checkPassword($inputs['username'], $inputs['password']) == FALSE) {
                    if (!isset($this->session->try_times)) {
                        $this->session->try_times = 0;
                    }
                    $this->session->try_times++;
                    if ($this->session->try_times > 5) {
                        $result = [
                            'ret' => 4,
                            'msg' => '账号已锁定.',
                        ];
                        $rows   = [
                            'lockuntil' => time() + 60 * 20,
                        ];
                        DB::table('admin')->where('username', '=', $inputs['username'])->update($rows);
                        setcookie('lockFlag', 1, time() + 60 * 20);
                        $this->session->del('try_times');
                    } else {
                        $result = [
                            'ret' => 5,
                            'msg' => '密码有误,剩余' . (5 - $this->session->try_times) . '次',
                        ];
                    }
                    break;
                }
                if ($data = $sysusers->setUserLogin($inputs['username'], $inputs['password'])) {
                    $this->session->del('try_times');
                    $result = [
                        'ret'  => 0,
                        'msg'  => '登陆成功.',
                        'data' => $data
                    ];
                } else {
                    $result = [
                        'ret' => 6,
                        'msg' => '登陆失败.',
                    ];
                }
            } while (FALSE);
            json($result);
        }
    }

}
